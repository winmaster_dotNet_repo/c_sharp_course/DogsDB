﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public partial class FormAddInfo : Form
    {
        public FormAddInfo()
        {
            InitializeComponent();
        }

        public TextBox TextBoxInfo
        {
            get
            {
                return textBoxInfo;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
