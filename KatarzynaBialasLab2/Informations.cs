﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab2
{
    [Serializable]
    public class Informations 
    {
        public string Value { get; set; }

        public Informations(string value)
        {
            this.Value = value;
        }
    }
}
