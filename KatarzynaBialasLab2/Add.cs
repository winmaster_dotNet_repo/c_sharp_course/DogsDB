﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public partial class FormAddDog : Form
    {
        public FormAddDog()
        {
            InitializeComponent();
        }

        public TextBox TextBoxName
        {
            get
            {
                return textBoxName;
            }
        }

        public TextBox TextBoxLand
        {
            get
            {
                return textBoxLand;
            }
        }

        public TextBox TextBoxSize
        {
            get
            {
                return textBoxSize;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
