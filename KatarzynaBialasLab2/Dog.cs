﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab2
{
    [Serializable]
       public class Dog
    {    
       public string Name { get; set; }
       public string Land { get; set; }
       public int Size { get; set; }
       public List<Informations> Informations  { get;  set;} 

        public Dog()
        {

        }

        public Dog(string name, string land, int size)
        {
            this.Name = name;
            this.Land = land;
            this.Size = size;
            Informations = new List<Informations>();
        }

        public void addInfo(string info)
        {
            Informations.Add(new Informations(info));
        }
    }
}
